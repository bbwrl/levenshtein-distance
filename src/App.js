import {useState} from "react";
import {Button, Col, Container, Form, Row} from "react-bootstrap";

function App() {
  const [word1, setWord1] = useState("Honda")
  const [word2, setWord2] = useState("Hyundai")
  const [statistics, setStatistics] = useState({})

  function buttonHandler() {
    let distance = levenshteinDistance(word1, word2)
    let statistics = {}
    statistics.found = distance>0
    statistics.distance = distance
    setStatistics(statistics)
  }

  return (
    <Container>
      <Row>
        <Col>
          <h1>Levenshtein Distance</h1>
        </Col>
      </Row>
      <Row>
        <Col>
          <Form.Group>
            <Form.Label><strong>Word 1</strong></Form.Label>
            <Form.Control type="text"
                          placeholder="Enter the first word"
                          value={word1}
                          onChange={e => setWord1(e.target.value)}/>
            <Form.Text className="text-muted">
            </Form.Text>
          </Form.Group>
          <Form.Group style={{marginTop: 30}}>
            <Form.Label><strong>Word 2</strong></Form.Label>
            <Form.Control type="text"
                          placeholder="Enter the first word"
                          value={word2}
                          onChange={e => setWord2(e.target.value)}/>
            <Form.Text className="text-muted">
            </Form.Text>
          </Form.Group>
          <Button onClick={buttonHandler} style={{marginTop: 20}}>calculate</Button>
        </Col>
      </Row>
      <Row>
        <Col>
          <hr/>
          <strong>statistics</strong>
          { statistics.found &&
            <div>
              <div>distance: {statistics.distance}</div>
            </div>
          }
        </Col>
      </Row>
    </Container>
  )
}


const levenshteinDistance = (str1 = '', str2 = '') => {
  const track = Array(str2.length + 1).fill(null).map(() =>
    Array(str1.length + 1).fill(null));
  for (let i = 0; i <= str1.length; i += 1) {
    track[0][i] = i;
  }
  for (let j = 0; j <= str2.length; j += 1) {
    track[j][0] = j;
  }
  for (let j = 1; j <= str2.length; j += 1) {
    for (let i = 1; i <= str1.length; i += 1) {
      const indicator = str1[i - 1] === str2[j - 1] ? 0 : 1;
      track[j][i] = Math.min(
        track[j][i - 1] + 1, // deletion
        track[j - 1][i] + 1, // insertion
        track[j - 1][i - 1] + indicator, // substitution
      );
    }
  }
  return track[str2.length][str1.length];
};

export default App;
